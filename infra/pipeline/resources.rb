# frozen_string_literal: true

require 'cfndsl'

CloudFormation do
    params = {}
    external_parameters.each_pair do |key, val|
        key = key.to_sym
        params[key] = val
    end
    Description 'Sets up resource dependencies for the CI/CD pipeline'

    Transform 'AWS::Serverless-2016-10-31'

    # Parameter (prefix)
    Parameter(:prefix) do
        Type String
        AllowedPattern '[a-z]*[-a-z0-9]*'
        ConstraintDescription 'A lower case string between 3 and 16 characters'
        Description 'A lower case string between 3 and 16 characters'
        MinLength 3
        MaxLength 16
    end

    #
    ##
    ### S3 - Pipeline resource bucket & policy
    ##
    #

    # S3 Bucket - Pipeline Resource bucket
    S3_Bucket(:resourcebucket) do
        BucketName FnJoin('', [Ref(:prefix), '-pipeline-resources'])
        VersioningConfiguration(
            Status: 'Enabled'
        )
        # Add tags. Skip TIER tag, always tagged as Production
        params[:tags].each_pair do |k, v|
            next if k == 'TIER'

            add_tag(k, v)
        end
        add_tag('TIER', 'Production')
    end
end
