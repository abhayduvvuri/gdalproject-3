export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY
export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_KEY
export AWS_DEFAULT_REGION=ap-southeast-2
aws ecr get-login --no-include-email
aws ecr get-login-password --region ap-southeast-2 | docker login --username AWS --password-stdin $AWS_ACCOUNT_ID.dkr.ecr.ap-southeast-2.amazonaws.com  > login.sh
cat login.sh
cat /root/.docker/config.json
aws ecr describe-repositories
cd ./infra/app/reprojectionImage
docker pull $AWS_ACCOUNT_ID.dkr.ecr.ap-southeast-2.amazonaws.com/$REPROJECTIMAGE:latest || true
docker build -t $AWS_ACCOUNT_ID.dkr.ecr.ap-southeast-2.amazonaws.com/$REPROJECTIMAGE:latest . --build-arg BASE_IMAGE=$AWS_ACCOUNT_ID.dkr.ecr.ap-southeast-2.amazonaws.com/$GDALBASEIMAGE
docker push $AWS_ACCOUNT_ID.dkr.ecr.ap-southeast-2.amazonaws.com/$REPROJECTIMAGE:latest
cd -