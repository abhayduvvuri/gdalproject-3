from osgeo import ogr

## Shapefile available?
driverName = "FileGDB"
drv = ogr.GetDriverByName( driverName )
if drv is None:
    print("{0} driver not configured.\n".format(driverName))
else:
    print("{0} driver configured.\n".format(driverName))
