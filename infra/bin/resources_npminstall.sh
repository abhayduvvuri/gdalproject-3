#!/usr/bin/env sh

for path in $( find "${CODEBUILD_SRC_DIR}/infra/pipeline/resources" -name resource.js | grep -v node_modules | sed "s[/resource.js[[" )
do
  cd "${path}" || exit 1
  npm install --only=prod
done
