# frozen_string_literal: true

require 'cfndsl'

CloudFormation do
    params = {}
    external_parameters.each_pair do |key, val|
        key = key.to_sym
        params[key] = val
    end
    Description 'ECR for Codebuild'

    ECR_Repository(:ecrrepo) do
        RepositoryName params[:prefix]
        RepositoryPolicyText(
            Version: '2012-10-17',
            Statement: [
                {
                    Sid: 'CodeBuildAccess',
                    Effect: 'Allow',
                    Principal: {
                        Service: 'codebuild.amazonaws.com'
                    },
                    Action: [
                        'ecr:GetDownloadUrlForLayer',
                        'ecr:BatchGetImage',
                        'ecr:BatchCheckLayerAvailability'
                    ]
                }
            ]
        )
    end
end
