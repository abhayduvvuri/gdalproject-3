from osgeo import ogr, osr
import os
import json
import xml.etree.ElementTree as ET

print('Reprojecting GDB')
filename = 'src'

gdb_name = '/data/{}.gdb'.format(filename)
NO_GEOMETRY = 100
valid_srid = ['4283', '7844']

if os.path.isdir(gdb_name):
    print("Data downloaded")
    gdbDriver = None
    inDataSet = None

    # output SpatialReference
    outSpatialRef = osr.SpatialReference()
    outSpatialRef.ImportFromEPSG(4283)

    source = gdb_name

    inDataSet = ogr.Open(source)
    outDataSet = None

    if inDataSet.GetLayerCount() > 0:

        # check for srid
        srid = 0
        for featsClass_idx in range(inDataSet.GetLayerCount()):
            inLayer = inDataSet.GetLayerByIndex(featsClass_idx)
            s_ref = inLayer.GetSpatialRef()
            if s_ref:
                srid = s_ref.GetAuthorityCode(None)

        if str(srid) in valid_srid:
            print("Dataset with srid %s need not be processed" % (str(srid)))
        else:
            dest = '/data/{}-proj.gdb'.format(filename)
            gdbDriver = ogr.GetDriverByName("FileGDB")
            outDataSet = gdbDriver.CreateDataSource(dest)        

            # parsing layers by index
            for featsClass_idx in range(inDataSet.GetLayerCount()):
                try:
                    inLayer = inDataSet.GetLayerByIndex(featsClass_idx)
                    geom_type = inLayer.GetGeomType()
                    lyr_name = inLayer.GetName()
                    outLayer = None
                    
                    if geom_type == NO_GEOMETRY:
                        print("table - {0} is ignored for processing".format(lyr_name))
                        #outLayer = outDataSet.CreateLayer(lyr_name, geom_type=ogr.wkbNone, srs=None)
                    else:
                        print("Creating layer - {0}".format(lyr_name))
                        outLayer = outDataSet.CreateLayer(lyr_name, geom_type=geom_type, srs=outSpatialRef)

                        print("Number of features in %s : %d" % (inLayer.GetName(),inLayer.GetFeatureCount()))

                        # add fields
                        inLayerDefn = inLayer.GetLayerDefn()
                        for i in range(0, inLayerDefn.GetFieldCount()):
                            field_name = inLayerDefn.GetFieldDefn(i).GetNameRef()
                            fieldDefn = inLayerDefn.GetFieldDefn(i)
                            outLayer.CreateField(fieldDefn)
                        
                        # get the output layer's feature definition
                        outLayerDefn = outLayer.GetLayerDefn()

                        # input SpatialReference
                        inSpatialRef = osr.SpatialReference()
                        inSpatialRef.ImportFromEPSG(int(srid))
                                    
                        # loop through the input features
                        inFeature = inLayer.GetNextFeature()
                        while inFeature:
                            if geom_type != NO_GEOMETRY:

                                # create the CoordinateTransformation
                                coordTrans = osr.CoordinateTransformation(inSpatialRef, outSpatialRef)
                                # get the input geometry
                                geom = inFeature.GetGeometryRef()

                                if geom is not None:
                                    # reproject the geometry
                                    geom.Transform(coordTrans)
                                    # create a new feature
                                    outFeature = ogr.Feature(outLayerDefn)
                                    # set the geometry and attribute
                                    outFeature.SetGeometry(geom)

                                    for i in range(0, outLayerDefn.GetFieldCount()):
                                        outFeature.SetField(outLayerDefn.GetFieldDefn(i).GetNameRef(), inFeature.GetField(i))

                                    # add the feature
                                    outLayer.CreateFeature(outFeature)
                                                    
                            # dereference the features and get the next input feature
                            outFeature = None
                            inFeature = inLayer.GetNextFeature()
                        print("Number of features written in %s : %d" % (outLayer.GetName(),outLayer.GetFeatureCount()))
                except Exception as e:
                    print(e)

    # Save and close the shapefiles
    inDataSet = None
    outDataSet = None
