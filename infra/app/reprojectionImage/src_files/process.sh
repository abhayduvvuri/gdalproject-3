python /data/test_driver.py
aws s3 cp $SRC_FOLDER/${FILENAME}.gdb/ /data/src.gdb --recursive
python /data/process.py
if [ -d "/data/src-proj.gdb" ]
then
  aws s3 cp /data/src-proj.gdb $DEST_FOLDER/${FILENAME}.gdb/ --recursive
fi