#!/usr/bin/env sh

for path in $( find "${CODEBUILD_SRC_DIR}/src/js" -name index.js | grep -v node_modules | sed "s[/index.js[[" )
do
  cd "${path}" || exit 1
  rm -rf node_modules
  npm install --only=prod --unsafe-perm || exit 1
  chmod -R 755 *
done
